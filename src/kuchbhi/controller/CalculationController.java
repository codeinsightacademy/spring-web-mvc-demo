package kuchbhi.controller;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

class Student {
	
	String name, city;
	int age;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Name: " + this.name + " Age: " + this.age + " City: " + this.city;
	}
	
	
	
}

@Controller
public class CalculationController {

	@RequestMapping("kuchbhi")
	public ModelAndView sqr() {
		
		System.out.println("I am in Controller's sqr method...");
		
		return new ModelAndView("test");
		
	}
	
	
	@RequestMapping("dhinchakpooja")
	public ModelAndView cubeofnumber() {
		
		System.out.println("I am in Controller's cubeofnumber...");
		
		System.out.println("\n\n==========================================\n\n");
		
		Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
        
		StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());

		SessionFactory factory = cfg.buildSessionFactory(ssrb.build());

		Session session = factory.getCurrentSession();
		
		String sql = "SELECT * from students";
		
		session.beginTransaction();
		
		SQLQuery query = session.createSQLQuery(sql);
		
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		
		List list = query.list();
		
		System.out.println(list);
		
		System.out.println("\n\n==========================================\n\n");
		
		session.close();
		
		//return new ModelAndView("dhinchakpoojaview.jsp");
		
		ModelAndView mv = new ModelAndView("student_list.jsp");
		
		mv.addObject("institute_name", "Code Insight Academy");
		
		mv.addObject("std_list", list);
		
		return mv;
		
	}
	
	
	@RequestMapping("input_query_string")
	public String inputGet(Model m, @RequestParam("num") int x) {
		
		m.addAttribute("result", x * x);
		
		return "input_get.jsp";
	}
	
	@RequestMapping(value = "add_action", method = RequestMethod.POST)
	public String addMethod(Model m, @RequestParam("num1") int x, @RequestParam("num2") int y) {
		
		m.addAttribute("result", x + y);
		
		return "add_result.jsp";
	}
	
	@RequestMapping(value="student_form_action", method = RequestMethod.POST)
	//public String addStudentRecord(ModelMap mm, @RequestParam("name") String name, @RequestParam("age") int age, @RequestParam("city") String city) {
	public String addStudentRecord(ModelMap mm, @ModelAttribute("student") Student std) {	
		mm.addAttribute("name", std.name);
		mm.addAttribute("age", std.age);
		mm.addAttribute("city", std.city);
		System.out.println(std);
		
		Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
        
		StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());

		SessionFactory factory = cfg.buildSessionFactory(ssrb.build());

		Session session = factory.getCurrentSession();
		
		String sql = "INSERT INTO `students` (`id`, `name`, `age`, `city`) VALUES(null, '"+ std.name +"', '"+std.age+"', '"+ std.city +"')";
		
		session.beginTransaction();
		
		SQLQuery query = session.createSQLQuery(sql);
		
		//query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		
		System.out.println("Query Result: " + query.executeUpdate());
		
		session.close();
		
		return "add_success.jsp";
	}
}
