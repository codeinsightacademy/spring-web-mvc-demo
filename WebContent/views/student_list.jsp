<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<h1>Student List</h1>

<div>${institute_name}</div>


<div>${std_list}</div>

<table border="1">
	<thead>
		<th>No.</th>
		<th>Name</th>
		<th>Age</th>
		<th>City</th>
	</thead>
	<c:forEach var = "std" items="${std_list}">
	<tr>
		<td>${std.id}</td>
		<td>${std.name}</td>
		<td>${std.age}</td>
		<td>${std.city}</td>
	</tr>
	</c:forEach>
</table>



</body>
</html>